int sensor=0;
void setup(){
Serial.begin(9600);//Configuración de la velocidad del puerto serial de arduino
//Se configuraran los pines 2,3,4,5 y 6 como salidas 
 pinMode(2, OUTPUT);
 pinMode(3, OUTPUT);
 pinMode(4, OUTPUT);
 pinMode(5, OUTPUT);
 pinMode(6, OUTPUT);
 
}
 
void loop(){
sensor=analogRead(A0);//Se lee el valor de la entrada analógica  A0
Serial.println(sensor);//Se imprime su valor por el terminal serial
//Se compara el valor de la variable sensor si se cumple apagará todos los led
if(sensor<99){
  digitalWrite(2, LOW);
  digitalWrite(3, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
 
  }
//Se compara el valor de la variable sensor si se cumple encenderá el led en el pin 2
if(sensor>100){
  digitalWrite(2, HIGH);
  digitalWrite(3, LOW);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
 
  }
//Se compara el valor de la variable sensor si se cumple encenderá el led en el pin 2 y 3
if(sensor>200){
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(4, LOW);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
 
  }
//Se compara el valor de la variable sensor si se cumple encenderá el led en el pin 2, 3 y 4
if(sensor>300){
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(4, HIGH);
  digitalWrite(5, LOW);
  digitalWrite(6, LOW);
 
  }
//Se compara el valor de la variable sensor si se cumple encenderá el led en el pin 2, 3, 4, y 5
if(sensor>350){
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);
  digitalWrite(6, LOW);
 
  }
//Se compara el valor de la variable sensor si se cumple encenderá el led en el pin 2, 3 ,4 ,5 y 6
if(sensor>400){
  digitalWrite(2, HIGH);
  digitalWrite(3, HIGH);
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
 
  }         
delay(100);//pequeño retardo antes de comenzar de vuelta
}
